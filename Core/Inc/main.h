/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Board_LED_Pin GPIO_PIN_13
#define Board_LED_GPIO_Port GPIOC
#define STBY_Pin GPIO_PIN_0
#define STBY_GPIO_Port GPIOA
#define SPI1_NCS_Pin GPIO_PIN_4
#define SPI1_NCS_GPIO_Port GPIOA
#define SPEED_1_Pin GPIO_PIN_0
#define SPEED_1_GPIO_Port GPIOB
#define SPEED_1_EXTI_IRQn EXTI0_IRQn
#define SPEED_2_Pin GPIO_PIN_1
#define SPEED_2_GPIO_Port GPIOB
#define SPEED_2_EXTI_IRQn EXTI1_IRQn
#define MOTOR_A_1_Pin GPIO_PIN_10
#define MOTOR_A_1_GPIO_Port GPIOB
#define MOTOR_A_2_Pin GPIO_PIN_11
#define MOTOR_A_2_GPIO_Port GPIOB
#define MOTOR_B_1_Pin GPIO_PIN_12
#define MOTOR_B_1_GPIO_Port GPIOB
#define MOTOR_B_2_Pin GPIO_PIN_13
#define MOTOR_B_2_GPIO_Port GPIOB
#define POWER_LED_1_Pin GPIO_PIN_14
#define POWER_LED_1_GPIO_Port GPIOB
#define POWER_LED_2_Pin GPIO_PIN_15
#define POWER_LED_2_GPIO_Port GPIOB
#define POWER_LED_3_Pin GPIO_PIN_8
#define POWER_LED_3_GPIO_Port GPIOA
#define POWER_LED_4_Pin GPIO_PIN_11
#define POWER_LED_4_GPIO_Port GPIOA
#define POWER_LED_5_Pin GPIO_PIN_12
#define POWER_LED_5_GPIO_Port GPIOA
#define SPEED_3_Pin GPIO_PIN_4
#define SPEED_3_GPIO_Port GPIOB
#define SPEED_3_EXTI_IRQn EXTI4_IRQn
#define SPEED_4_Pin GPIO_PIN_5
#define SPEED_4_GPIO_Port GPIOB
#define SPEED_4_EXTI_IRQn EXTI9_5_IRQn
#define MOTOR_SWITCH_Pin GPIO_PIN_8
#define MOTOR_SWITCH_GPIO_Port GPIOB
#define VISION_Pin GPIO_PIN_9
#define VISION_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
int fputc(int ch,FILE *f);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
