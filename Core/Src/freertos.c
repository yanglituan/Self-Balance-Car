/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"

#include "cmd.h"
#include "cmsis_os.h"
#include "compass.h"
#include "control.h"
#include "imu.h"
#include "main.h"
#include "maintask.h"
#include "power.h"
#include "speed.h"
#include "task.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for mainTask */
osThreadId_t mainTaskHandle;
const osThreadAttr_t mainTask_attributes = {
    .name = "mainTask",
    .priority = (osPriority_t)osPriorityNormal,
    .stack_size = 128 * 4};
/* Definitions for MotorSpeed */
osThreadId_t MotorSpeedHandle;
const osThreadAttr_t MotorSpeed_attributes = {
    .name = "MotorSpeed",
    .priority = (osPriority_t)osPriorityLow,
    .stack_size = 128 * 4};
/* Definitions for MotorControl */
osThreadId_t MotorControlHandle;
const osThreadAttr_t MotorControl_attributes = {
    .name = "MotorControl",
    .priority = (osPriority_t)osPriorityLow,
    .stack_size = 128 * 4};
/* Definitions for IMU */
osThreadId_t IMUHandle;
const osThreadAttr_t IMU_attributes = {.name = "IMU",
                                       .priority = (osPriority_t)osPriorityLow,
                                       .stack_size = 128 * 4};
/* Definitions for COMPASS */
osThreadId_t COMPASSHandle;
const osThreadAttr_t COMPASS_attributes = {
    .name = "COMPASS",
    .priority = (osPriority_t)osPriorityLow,
    .stack_size = 128 * 4};
/* Definitions for PowerMonitor */
osThreadId_t PowerMonitorHandle;
const osThreadAttr_t PowerMonitor_attributes = {
    .name = "PowerMonitor",
    .priority = (osPriority_t)osPriorityLow,
    .stack_size = 128 * 4};
/* Definitions for CMD */
osThreadId_t CMDHandle;
const osThreadAttr_t CMD_attributes = {.name = "CMD",
                                       .priority = (osPriority_t)osPriorityLow,
                                       .stack_size = 128 * 4};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void main_Task(void *argument);
void Motor_Speed(void *argument);
void Motor_Control(void *argument);
void MPU6500(void *argument);
void HMC5883L(void *argument);
void Power_Monitor(void *argument);
void CMD_Receive(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of mainTask */
  mainTaskHandle = osThreadNew(main_Task, NULL, &mainTask_attributes);

  /* creation of MotorSpeed */
  MotorSpeedHandle = osThreadNew(Motor_Speed, NULL, &MotorSpeed_attributes);

  /* creation of MotorControl */
  MotorControlHandle =
      osThreadNew(Motor_Control, NULL, &MotorControl_attributes);

  /* creation of IMU */
  IMUHandle = osThreadNew(MPU6500, NULL, &IMU_attributes);

  /* creation of COMPASS */
  COMPASSHandle = osThreadNew(HMC5883L, NULL, &COMPASS_attributes);

  /* creation of PowerMonitor */
  PowerMonitorHandle =
      osThreadNew(Power_Monitor, NULL, &PowerMonitor_attributes);

  /* creation of CMD */
  CMDHandle = osThreadNew(CMD_Receive, NULL, &CMD_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
