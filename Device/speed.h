#include <stdint.h>

#define SPEED_SCAN_FREQ (50.0f)
#define SPEED_MAX_PULSE_NUMBER (390.0f) /* 车轮转动一圈产生脉冲数量 */
#define SPEED_SENSOR_NUMBER (2.0f)      /* 每个车轮的编码器数量 */

/**
 * @brief 电机初始化
 *
 */
void Speed_Init(void);

/**
 * @brief 电机测速主程序
 *
 */
void Speed_Scan(void);
