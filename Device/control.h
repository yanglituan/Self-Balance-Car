#include <stdbool.h>
#include <stdint.h>

#define CONTROL_FREQ (200)

/**
 * @brief 应用PID控制
 *
 */
void Control_Apply(void);

/**
 * @brief 控制初始化
 *
 */
void Control_Init(void);
