#include <stdint.h>

#include "main.h"

#define CMD_SCAN_FREQ (100)
#define BUFFER_SIZE (10) /* 缓冲数组大小 */

void Uart_IRQHandler(void);

/**
 * @brief DMA串口发送数据
 *
 * @param buf 数据地址
 * @param len 数据长度
 * @return uint8_t
 */
uint8_t Uart_SendDMA(uint8_t *buf, uint8_t len);

/**
 * @brief 开启串口DMA
 *
 * @return uint8_t
 */
uint8_t Uart_StartDMA(void);

/**
 * @brief 初始化串口
 *
 * @param handle 串口句柄
 * @param dma_tx DMA发送句柄
 * @param dma_rx DMA接收句柄
 */
void Uart_Init(UART_HandleTypeDef *handle, DMA_HandleTypeDef *dma_tx,
               DMA_HandleTypeDef *dma_rx);

/**
 * @brief 检查是否有串口数据
 *
 * @return uint8_t
 */
uint8_t Uart_CheckFlag(void);

/**
 * @brief 获取数据地址
 *
 * @return uint8_t*
 */
uint8_t *Uart_GetDataAddr(void);

/**
 * @brief 获取数据长度
 *
 * @return uint8_t
 */
uint8_t Uart_GetDataLen(void);

/**
 * @brief 命令解析
 *
 * @param data 数据地址
 * @param len 数据长度
 */
void Prase_CMD(uint8_t *data, uint8_t len);
