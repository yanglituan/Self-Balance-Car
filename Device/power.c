#include "../Device/power.h"

#include "main.h"

void Power_Init(ADC_HandleTypeDef* adc, uint32_t* buff) {
  HAL_ADC_Start_DMA(adc, buff, POWER_SCAN_NUM);
}

void Power_Analy(uint32_t* buff, float* precentage) {
  float tmp = 0;
  for (uint16_t i = 0; i < POWER_SCAN_NUM; i++) {
    tmp += (float)buff[i] / (float)POWER_SCAN_NUM;
  }
  tmp = tmp / 4096.0f * 3.3f / POWER_VOLTAGE_RATIO / POWER_BAT_NUM;
  *precentage =
      (tmp - POWER_MIN_VOLTAGE) / (POWER_MAX_VOLTAGE - POWER_MIN_VOLTAGE);
  if (*precentage > 1.0f) *precentage = 1;
  if (*precentage < 0.0f) *precentage = 0;
}
