#include <stdint.h>

#define COMPASS_SCAN_FREQ (25)

/**
 * @brief 罗盘初始化
 *
 */
void Compass_Init(void);

/**
 * @brief 获取罗盘数据
 *
 */
void Compass_GetData(void);
