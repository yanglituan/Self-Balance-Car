#include <stdint.h>

#include "main.h"

#define POWER_SCAN_FREQ (1.0f)
#define POWER_SCAN_NUM (10)      /* 采集次数 */
#define POWER_MAX_VOLTAGE (4.2f) /* 亏电电压 */
#define POWER_MIN_VOLTAGE (3.2f) /* 满电电压 */
#define POWER_BAT_NUM (3)        /* 电池数量 */
/* 分压比例，由板上电阻决定，电压不能超过3.3V(血的教训.jpg) */
#define POWER_VOLTAGE_RATIO (0.16667)

/**
 * @brief 开启第一次AD转换
 *
 * @param adc ADC handle
 */
void Power_Init(ADC_HandleTypeDef* adc, uint32_t* buff);

/**
 * @brief 电量计算函数
 *
 * @param buff 存放ADC值的数组
 * @param precentage 电量百分比
 */
void Power_Analy(uint32_t* buff, float* precentage);
