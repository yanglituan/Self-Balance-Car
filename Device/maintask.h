#include <stdint.h>

#define MAINTASK_FREQ (50)

typedef enum { STATUS_STOP, STATUS_WORK } Status_t;

/**
 * @brief 更新LED状态
 *
 */
void Update_LED(void);

/**
 * @brief 报告信息
 *
 */
void Report_Status(void);
