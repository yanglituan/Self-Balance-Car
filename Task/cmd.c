#include "../Device/cmd.h"

#include "../Task/cmd.h"
#include "sys.h"

extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart1_tx;

/**
 * @brief 接收并解析遥控命令
 * @param argument: 未使用
 * @retval 无
 */
void CMD_Receive(void *argument) {
  uint8_t *data;
  uint8_t len;
  const uint32_t delay_tick = osKernelGetTickFreq() / CMD_SCAN_FREQ;

  Uart_Init(&huart1, &hdma_usart1_rx, &hdma_usart1_tx);

  uint32_t tick = osKernelGetTickCount();
  while (1) {
    tick += delay_tick;

    if (Uart_CheckFlag()) {
      data = Uart_GetDataAddr();
      len = Uart_GetDataLen();
      Prase_CMD(data, len);
    }

    osDelayUntil(tick);
  }
}
