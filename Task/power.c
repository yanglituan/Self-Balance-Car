#include "../Task/power.h"

#include "../Device/power.h"
#include "sys.h"

float power_precentage;
uint32_t AD_Value[POWER_SCAN_NUM];
extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

/**
 * @brief 监控电池状态
 * @param argument: 未使用
 * @retval 无
 */
void Power_Monitor(void *argument) {
  const uint32_t delay_tick = osKernelGetTickFreq() / POWER_SCAN_FREQ;

  Power_Init(&hadc1, AD_Value);

  uint32_t tick = osKernelGetTickCount();
  while (1) {
    tick += delay_tick;

    Power_Analy(AD_Value, &power_precentage);

    osDelayUntil(tick);
  }
}
