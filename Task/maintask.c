#include "../Task/maintask.h"

#include <stdio.h>

#include "../Device/cmd.h"
#include "../Device/maintask.h"
#include "iwdg.h"
#include "sys.h"

extern IWDG_HandleTypeDef hiwdg;
extern float Pitch, Roll, Yaw;
/**
 * @brief  主控制进程
 * @param  argument: 未使用
 * @retval 无
 */
void main_Task(void *argument) {
  const uint32_t delay_tick = osKernelGetTickFreq() / MAINTASK_FREQ;

  MX_IWDG_Init();

  uint32_t tick = osKernelGetTickCount();
  while (1) {
    tick += delay_tick;

    HAL_IWDG_Refresh(&hiwdg);

    Update_LED();

    Report_Status();

    osDelayUntil(tick);
  }
}
