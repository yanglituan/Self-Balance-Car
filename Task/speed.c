#include "../Task/speed.h"

#include "../Device/speed.h"
#include "sys.h"

float motor_a_counter, motor_b_counter, motor_a_speed, motor_b_speed;
extern TIM_HandleTypeDef htim2;

/**
 * @brief 电机速度检测
 * @param argument: 未使用
 * @retval 无
 */
void Motor_Speed(void *argument) {
  const uint32_t delay_tick = osKernelGetTickFreq() / SPEED_SCAN_FREQ;
  uint32_t scan_counter = 0;

  Speed_Init();

  uint32_t tick = osKernelGetTickCount();

  while (1) {
    tick += delay_tick;

    scan_counter++;
    if (scan_counter >= 20) {
      Speed_Scan();
      scan_counter = 0;
    }

    osDelayUntil(tick);
  }
  /* USER CODE END Motor_Speed */
}
