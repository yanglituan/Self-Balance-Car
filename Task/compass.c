#include "../Task/compass.h"

#include "../Device/compass.h"
#include "ahrs.h"
#include "sys.h"

int16_t compass_data_x, compass_data_y, compass_data_z;
float compass_x, compass_y, compass_z;

/**
 * @brief 读取HMC5883L数据
 * @param argument: 未使用
 * @retval 无
 */
void HMC5883L(void *argument) {
  const uint32_t delay_tick = osKernelGetTickFreq() / COMPASS_SCAN_FREQ;

  Compass_Init();

  uint32_t tick = osKernelGetTickCount();
  while (1) {
    tick += delay_tick;

    Compass_GetData();

    AHRS_Prase_Compass();

    AHRS_AnalyYaw();

    osDelayUntil(tick);
  }
}
