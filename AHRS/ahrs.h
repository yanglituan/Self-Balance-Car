#include "sys.h"

/**
 * @brief IMU数据换算
 *
 */
void AHRS_Prase_IMU(void);

/**
 * @brief 磁力计数据换算
 *
 */
void AHRS_Prase_Compass(void);

/**
 * @brief 计算欧拉角
 *
 */
void AHRS_AnalyAngle(void);

/**
 * @brief 计算Yaw
 *
 */
void AHRS_AnalyYaw(void);
