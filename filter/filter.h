#include <stdint.h>

#define MAX_DATA_NUMBER (20)

typedef struct {
  float data[MAX_DATA_NUMBER];
  float max;
  float min;
  uint8_t counter;
  uint8_t ready;
  uint8_t n;
} MedianAverageFilter_t;

typedef struct {
  float data[MAX_DATA_NUMBER];
  uint8_t counter;
  uint8_t ready;
  uint8_t n;
} AverageFilter_t;

typedef struct {
  float k;
  float out;
} LowPassFilter_t;

typedef struct {
  float k;
  float freq;
  float out;
} ComplementaryFilter_t;

/**
 * @brief 中位值平均滤波器初始化
 *
 * @param filter 滤波器结构体
 * @param n 数据数量
 * @return int8_t
 */
int8_t MedianAverageFilter_Init(MedianAverageFilter_t* filter, uint8_t n);
/**
 * @brief 施加一次滤波计算
 *
 * @param filter 滤波器结构体
 * @param new 新数据
 * @return float 滤波后数据
 */
float MedianAverageFilter_Apply(MedianAverageFilter_t* filter, float new);

/**
 * @brief 平均滤波器初始化
 *
 * @param filter 滤波器结构体
 * @param n 数据数量
 * @return int8_t
 */
int8_t AverageFilter_Init(AverageFilter_t* filter, uint8_t n);
/**
 * @brief 施加一次滤波计算
 *
 * @param filter 滤波器结构体
 * @param new 新数据
 * @return float 滤波后数据
 */
float AverageFilter_Apply(AverageFilter_t* filter, float new);

/**
 * @brief 低通滤波器初始化
 *
 * @param filter 滤波器结构体
 * @param n 参数
 * @return int8_t
 */
int8_t LowPassFilter_Init(LowPassFilter_t* filter, float k);
/**
 * @brief 施加一次滤波计算
 *
 * @param filter 滤波器结构体
 * @param new 新数据
 * @return float 滤波后数据
 */
float LowPassFilter_Apply(LowPassFilter_t* filter, float new);

/**
 * @brief 互补滤波器初始化
 *
 * @param filter 滤波器
 * @param k 参数
 * @param freq 频率
 * @return int8_t
 */
int8_t ComplementaryFilter_Init(ComplementaryFilter_t* filter, float k,
                                uint32_t freq);
/**
 * @brief 施加一次滤波计算
 *
 * @param filter 滤波器
 * @param angle 计算加速度得到的角度
 * @param gyro 陀螺仪数据
 * @return float 滤波后数据
 */
float ComplementaryFilter_Apply(ComplementaryFilter_t* filter, float angle,
                                float gyro);
