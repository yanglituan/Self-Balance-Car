void delay_init(int sys_clk);
void delay_us(int uSec);

#define delay_ms(mSec) delay_us(mSec * 1000)
